import enum
import logging
import urllib.parse
import warnings
from typing import Optional, Iterator, Tuple, List, Union

import requests

from .models import (Account,
                     AccountRef,
                     AccountEmailAddresses,
                     AccountPassword,
                     AccountQuarantines,
                     Affiliation,
                     Contact,
                     Consent,
                     Group,
                     GroupListItem,
                     GroupMember,
                     OU,
                     PersonExternalId,
                     PersonExternalIdSearchResult,
                     Person,
                     Trait)

logger = logging.getLogger(__name__)


class Response(enum.Enum):
    OK = enum.auto()
    UPDATED = enum.auto()
    CREATED = enum.auto()
    ILLEGAL_NAME = enum.auto()
    NOT_FOUND = enum.auto()
    ERROR = enum.auto()


class Visibility(enum.Enum):
    ALL = 'all'
    INTERNAL = 'internal'
    NONE = 'none'


def quote_path_arg(arg: str) -> str:
    return urllib.parse.quote_plus(str(arg))


def urljoin(base_url: str, *paths) -> str:
    """An urljoin that takes multiple path arguments.

    Note how urllib.parse.urljoin will assume 'relative to parent' when the
    base_url doesn't end with a '/':

    >>> urllib.parse.urljoin('https://localhost/foo', 'bar')
    'https://localhost/bar'

    >>> urljoin('https://localhost/foo', 'bar')
    'https://localhost/foo/bar'

    >>> urljoin('https://localhost/foo', 'bar', 'baz')
    'https://localhost/foo/bar/baz'
    """
    for path in paths:
        base_url = urllib.parse.urljoin(base_url + '/', path)
    return base_url


def merge_dicts(*dicts) -> dict:
    """
    Combine a series of dicts without mutating any of them.

    >>> merge_dicts({'a': 1}, {'b': 2})
    {'a': 1, 'b': 2}
    >>> merge_dicts({'a': 1}, {'a': 2})
    {'a': 2}
    >>> merge_dicts(None, None, None)
    {}
    """
    combined = dict()
    for d in dicts:
        if not d:
            continue
        for k in d:
            combined[k] = d[k]
    return combined


class CerebrumEndpoints:

    def __init__(self, url):
        self.baseurl = url

    def __repr__(self):
        return '{cls.__name__}({url!r})'.format(
            cls=type(self),
            url=self.baseurl)

    def get_href(self, href):
        return urljoin(self.baseurl, href)

    def get_account_url(self, account):
        name = quote_path_arg(account)
        return urljoin(self.baseurl, 'v1',
                       'accounts/{}'.format(name))

    def get_search_url(self):
        return urljoin(self.baseurl, 'v1', 'search/')

    def get_account_email_url(self, account):
        return urljoin(self.get_account_url(account),
                       'emailaddresses')

    def get_account_groups_url(self, account):
        return urljoin(self.get_account_url(account),
                       'groups')

    def get_account_traits_url(self, account):
        return urljoin(self.get_account_url(account),
                       'traits')

    def get_account_quarantines_url(self, account):
        return urljoin(self.get_account_url(account),
                       'quarantines')

    def get_person_url(self, person_id):
        ident = quote_path_arg(str(person_id))
        return urljoin(self.baseurl, 'v1',
                       'persons/{}'.format(ident))

    def get_person_accounts_url(self, person_id):
        return urljoin(self.get_person_url(person_id),
                       'accounts')

    def get_person_affiliations_url(self, person_id):
        return urljoin(self.get_person_url(person_id),
                       'affiliations')

    def get_person_identifiers_url(self, person_id):
        return urljoin(self.get_person_url(person_id),
                       'external-ids')

    def get_person_contact_url(self, person_id):
        return urljoin(self.get_person_url(person_id),
                       'contacts')

    def get_person_consents_url(self, person_id):
        return urljoin(self.get_person_url(person_id),
                       'consents')

    def get_person_otp_profile_url(self, person_id, otp_profile):
        return urljoin(self.get_person_url(person_id),
                       'otp/{}'.format(otp_profile))

    def get_ou_url(self, ou_id):
        return urljoin(self.baseurl, 'v1', 'ous/{}'.format(ou_id))

    def get_search_person_identifiers_url(self):
        return urljoin(self.baseurl, 'v1',
                       'search/persons/external-ids')

    def get_group(self, group_name):
        return urljoin(self.baseurl, 'v1', 'groups', group_name)

    def get_group_member(self, group_name, entity_id):
        return urljoin(self.get_group(group_name), 'members/{}'.format(
            entity_id))

    def list_group_members(self, group_name):
        return urljoin(self.get_group(group_name), 'members/')

    def set_password_url(self, account):
        return urljoin(self.get_account_url(account), 'password')

    def verify_password_url(self, account):
        return urljoin(self.get_account_url(account), 'password/verify')

    def check_password_url(self, account):
        return urljoin(self.get_account_url(account), 'password/check')

    def search_external_ids_url(self):
        return urljoin(self.get_search_url(), 'persons/external-ids')


class CerebrumClient:
    """
    The core Cerebrum client class
    """

    default_headers = {
        'Accept': 'application/json',
    }

    def __init__(self,
                 url: str,
                 headers: Optional[dict] = None,
                 rewrite_url: Optional[Tuple[str, str]] = None,
                 use_sessions: Union[bool, requests.Session] = True):
        self.urls = CerebrumEndpoints(url)
        self.headers = merge_dicts(self.default_headers, headers)
        self.rewrite_url = rewrite_url

        if use_sessions:
            if isinstance(use_sessions, requests.Session):
                self.session = use_sessions
            else:
                self.session = requests.Session()
        else:
            self.session = requests

    def _call(self,
              method_name: str,
              url: str,
              headers: Optional[dict] = None,
              return_response: bool = False, **kwargs):
        headers = merge_dicts(self.headers, headers)
        if self.rewrite_url is not None:
            url = url.replace(*self.rewrite_url)
        auth = None
        response = self.session.request(method_name,
                                        url,
                                        auth=auth,
                                        headers=headers,
                                        **kwargs)
        if return_response:
            return response
        else:
            if response.status_code in (500, 400):
                logger.warn(
                    'Got HTTP %d: %r', response.status_code, response.json())
            if response.status_code == 404:
                return None
            response.raise_for_status()
            return response

    def get(self, path, **kwargs):
        return self._call('GET', path, **kwargs)

    def put(self, path, **kwargs):
        return self._call('PUT', path, **kwargs)

    def post(self, path, **kwargs):
        return self._call('POST', path, **kwargs)

    def delete(self, path, **kwargs):
        return self._call('DELETE', path, **kwargs)

    def get_account(self, name) -> Optional[Account]:
        url = self.urls.get_account_url(name)
        response = self.get(url)
        if not response:
            return None
        return Account.from_dict(response.json())

    def get_account_emailaddresses(
        self, name
    ) -> Optional[AccountEmailAddresses]:

        url = self.urls.get_account_email_url(name)
        response = self.get(url)
        if not response:
            return None
        return AccountEmailAddresses.from_dict(response.json())

    def get_account_traits(self, name) -> Optional[Trait]:
        url = self.urls.get_account_traits_url(name)
        response = self.get(url)
        if not response:
            return None
        for x in response.json().get('traits', []):
            yield Trait.from_dict(x)

    def get_account_memberships(self, name) -> Optional[Iterator[Group]]:
        url = self.urls.get_account_groups_url(name)
        response = self.get(url)
        if not response:
            return None
        for x in response.json().get('groups', []):
            yield GroupListItem.from_dict(x)

    def get_account_quarantines(self, name) -> Optional[AccountQuarantines]:
        url = self.urls.get_account_quarantines_url(name)
        response = self.get(url)
        if not response:
            return None
        return AccountQuarantines.from_dict(response.json())

    def get_person(self, person_id) -> Optional[Person]:
        url = self.urls.get_person_url(person_id)
        response = self.get(url)
        if not response:
            return None
        return Person.from_dict(response.json())

    def get_person_accounts(self, person_id) -> Optional[Iterator[AccountRef]]:
        url = self.urls.get_person_accounts_url(person_id)
        response = self.get(url)
        if not response:
            return None
        for x in response.json().get('accounts', []):
            yield AccountRef.from_dict(x)

    def get_person_primary_account(self, person_id) -> Optional[Account]:
        primary = self.get_person_primary_account_name(person_id)
        if not primary:
            return None
        return self.get_account(primary)

    def get_person_primary_account_name(self, person_id) -> Optional[str]:
        accounts = self.get_person_accounts(person_id)
        if not accounts:
            return None
        primary = list(filter(lambda x: x.primary is True, accounts))
        if not primary:
            return None
        return primary.pop().name

    def get_person_external_ids(
        self, person_id
    ) -> Optional[Iterator[PersonExternalId]]:

        url = self.urls.get_person_identifiers_url(person_id)
        response = self.get(url)
        if not response:
            return None
        for x in response.json().get('external_ids', []):
            yield PersonExternalId.from_dict(x)

    def get_person_contact(self, person_id) -> Optional[Iterator[Contact]]:
        url = self.urls.get_person_contact_url(person_id)
        response = self.get(url)
        if not response:
            return None
        for x in response.json().get('contacts', []):
            yield Contact.from_dict(x)

    def get_person_consents(
        self,
        person_id
    ) -> Optional[Iterator[Consent]]:

        url = self.urls.get_person_consents_url(person_id)
        response = self.get(url)
        if not response:
            return None
        for x in response.json().get('consents', []):
            yield Consent.from_dict(x)

    def get_person_affiliations(
        self, person_id
    ) -> Optional[Iterator[Affiliation]]:

        url = self.urls.get_person_affiliations_url(person_id)
        response = self.get(url)
        if not response:
            return None
        for x in response.json().get('affiliations', []):
            yield Affiliation.from_dict(x)

    def get_ou(self, ou_id) -> Optional[OU]:
        url = self.urls.get_ou_url(ou_id)
        response = self.get(url)
        if not response:
            return None
        return OU.from_dict(response.json())

    def get_group(self, group_name) -> Optional[Group]:
        url = self.urls.get_group(group_name)
        response = self.get(url)
        if not response:
            return None
        return Group.from_dict(response.json())

    def ensure_group(
        self, group_name: str,
        description: str, visibility: Visibility = Visibility.ALL
    ) -> Response:

        url = self.urls.get_group(group_name)
        response = self.put(url,
                            return_response=True,
                            data={
                                'description': description,
                                'visibility': visibility.value
                            })
        if response.status_code == requests.codes.ok:
            return Response.UPDATED
        elif response.status_code == requests.codes.created:
            return Response.CREATED
        elif response.status_code == requests.codes.bad_request:
            return Response.ILLEGAL_NAME
        else:
            response.raise_for_status()

    def set_group_members(
        self, group_name: str, member_ids: List[int]
    ) -> Response:

        url = self.urls.list_group_members(group_name)
        response = self.put(url,
                            json={'members': member_ids},
                            return_response=True)
        if response.status_code == requests.codes.ok:
            return Response.OK
        elif response.status_code == requests.codes.not_found:
            return Response.NOT_FOUND
        else:
            response.raise_for_status()

    def set_account_password(
        self, account_name: str, password=None
    ) -> AccountPassword:

        url = self.urls.set_password_url(account_name)
        try:
            if password:
                response = self.post(url, json={'password': password},
                                     return_response=True)
            else:
                response = self.post(url, return_response=True)
            return response.json()
            # return bool(response.json().get('password', False))
        except requests.exceptions.HTTPError as e:
            if e.response.status_code == 400:
                return False
            raise

    def verify_account_password(self, account_name: str, password: str) -> bool:
        url = self.urls.verify_password_url(account_name)
        try:
            response = self.post(url, json={'password': password},
                                 return_response=True)
            return response.json().get('verified', False)
        except requests.exceptions.HTTPError as e:
            if e.response.status_code == 400:
                return False
            raise
        return False

    def check_account_password(self, account_name: str, password: str) -> bool:
        url = self.urls.check_password_url(account_name)
        try:
            response = self.post(url, json={'password': password},
                                 return_response=True)
            return response.json().get('passed', False)
        except requests.exceptions.HTTPError as e:
            if e.response.status_code == 400:
                return False
            raise
        return False

    def check_account_password_errors(self,
                                      account_name: str,
                                      password: str) -> List[str]:
        url = self.urls.check_password_url(account_name)
        response = self.post(url,
                             json={'password': password},
                             return_response=True)
        if response.status_code == 200:
            ret = []
            for style, errors in response.json()['checks'].items():
                for error in errors:
                    for error_name, error_info in error.items():
                        if not error_info['passed']:
                            ret.append(error_name)
            return ret
        elif response.status_code == 400:
            return ['bad_request']
        else:
            response.raise_for_status()

    def add_group_member(self, group_name: str, member_id: int) -> Response:
        url = self.urls.get_group_member(group_name, member_id)
        response = self.put(url, return_response=True)
        if response.status_code == requests.codes.ok:
            return Response.OK
        elif response.status_code == requests.codes.not_found:
            return Response.NOT_FOUND
        else:
            response.raise_for_status()

    def remove_group_member(self, group_name: str, member_id: int) -> Response:
        url = self.urls.get_group_member(group_name, member_id)
        response = self.delete(url, return_response=True)
        if response.status_code == requests.codes.no_content:
            return Response.OK
        elif response.status_code == requests.codes.not_found:
            return Response.NOT_FOUND
        else:
            response.raise_for_status()

    def list_group_members(
        self, group_name
    ) -> Optional[Iterator[GroupMember]]:

        url = self.urls.list_group_members(group_name)
        response = self.get(url)
        if not response:
            return None
        for x in response.json().get('members', []):
            yield GroupMember.from_dict(x)

    def is_group_member(self, group_name, entity_id) -> bool:
        url = self.urls.get_group_member(group_name, entity_id)
        response = self.get(url)
        if response and response.json().get('id') == int(entity_id):
            return True
        return False

    def search_person_external_ids(
        self,
        source_system=None,
        id_type=None,
        external_id=None
    ) -> Iterator[PersonExternalIdSearchResult]:

        url = self.urls.get_search_person_identifiers_url()
        params = {}
        if source_system:
            params['source_system'] = source_system
        if id_type:
            params['id_type'] = id_type
        if external_id:
            params['external_id'] = external_id
        response = self.get(url, params=params)
        for x in response.json().get('external_ids', []):
            yield PersonExternalIdSearchResult.from_dict(x)

    def set_otp_secret(
        self,
        person_id: int,
        otp_secret: str,
        otp_profile: str='default'
    ) -> bool:

        url = self.urls.get_person_otp_profile_url(person_id, otp_profile)
        response = self.put(url,
                            json={'secret': otp_secret},
                            return_response=True)
        if response.status_code == 204:
            return True
        elif response.status_code == 400:
            return False
        else:
            response.raise_for_status()

    def get_otp_status(
        self,
        person_id: int,
        otp_profile: str='default'
    ) -> bool:

        url = self.urls.get_person_otp_profile_url(person_id, otp_profile)
        response = self.get(url, return_response=True)
        if response.status_code == 200:
            return response.json()['has_secret']
        else:
            response.raise_for_status()

    def del_otp_secret(
        self,
        person_id: int,
        otp_profile: str='default'
    ) -> bool:

        url = self.urls.get_person_otp_profile_url(person_id, otp_profile)
        response = self.delete(url, return_response=True)
        if response.status_code == 204:
            return True
        elif response.status_code == 404:
            return False
        else:
            response.raise_for_status()


def get_client(config_dict) -> CerebrumClient:
    """
    Get a CerebrumClient from configuration.
    """
    return CerebrumClient(
        config_dict['url'],
        headers=config_dict.get('headers'),
        rewrite_url=config_dict.get('rewrite_url'),
    )
