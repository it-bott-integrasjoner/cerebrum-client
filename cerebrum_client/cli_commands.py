"""
Cli command implementation.

This module implements a cli client argument parser, and command
implementations.
"""
import argparse
import logging

from .version import get_distribution

logger = logging.getLogger(__name__)

default_config_file = 'config.json'

parser = argparse.ArgumentParser(
    formatter_class=argparse.RawDescriptionHelpFormatter,
    epilog="""
        Examples:
            python -m cerebrum_client get-account example
            python -m cerebrum_client GET /v1/accounts/example
    """.strip(),
)
parser.add_argument(
    '-v', '--version',
    action='version',
    version=str(get_distribution()),
)
parser.add_argument(
    '-c', '--config',
    dest='config',
    default=default_config_file,
    help='read config from %(metavar)s (default: %(default)s)',
    metavar='<json-file>',
)

# TODO: Should we make the data model optional, like sap-client?
#
# parser.add_argument(
#     '-r', '--raw',
#     dest='use_models',
#     action='store_false',
#     default=True,
#     help='Do not parse response objects',
# )


# subparser for commands
commands = parser.add_subparsers(
    title='commands',
    description='Lookups supported by this command line client',
    dest='command',
    metavar='<command>',
)


def add_command(command_name, **kwargs):
    """
    Add a subparser in `commands` for decorated function.

    :param command_name: a unique command name

    :returns callable:
        Decorator that adds function as a command implementation.
    """
    def wrapper(fn):
        kwargs.setdefault('help', fn.__doc__.strip())
        kwargs.setdefault('description', fn.__doc__.strip())
        subparser = commands.add_parser(command_name, **kwargs)
        subparser.set_defaults(cmd=fn)
        setattr(fn, 'name', command_name)
        setattr(fn, 'parser', subparser)
        return fn
    return wrapper


def run_command(client, args):
    """ Call a decorated command implementation from cli args. """
    logger.debug('run_command client=%r args=%r', client, args)
    result = args.cmd(client, args)
    if result is None:
        logger.warning('no result from %r', args.cmd.name)
    return result


#
# get-account <username>
#
@add_command('get-account')
def get_account(client, args):
    """ get account info from username """
    name = args.name
    logger.info('get-account: %r', name)

    return client.get_account(name)


get_account.parser.add_argument('name')


#
# get-group <group name>
#
@add_command('get-group')
def get_group(client, args):
    """ get group info from name """
    name = args.name
    logger.info('get-group: %r', name)
    return client.get_group(name)


get_group.parser.add_argument('name')


#
# get-group-members <group name>
#
@add_command('get-group-members')
def get_group_members(client, args):
    """ list members of a group with a given name """
    name = args.name
    logger.info('get-group-members: %r', name)
    return tuple(client.list_group_members(name))


get_group_members.parser.add_argument('name')


@add_command('get-person')
def get_person(client, args):
    """ get person info from person-id """
    person_id = args.id
    logger.info('get-person: %r', person_id)
    return client.get_person(person_id)


get_person.parser.add_argument('id')


#
# search-persons ...
#
@add_command('search-persons')
def search_persons(client, args):
    """ search persons by external id """
    search_args = {
        'source_system': args.source,
        'id_type': args.type,
        'external_id': args.value,
    }
    logger.info('get-group-members: %r', search_args)
    return tuple(client.search_person_external_ids(**search_args))


search_persons.parser.add_argument(
    '--source',
    help='filter by source system',
)
search_persons.parser.add_argument(
    '--type',
    help='filter by external id type',
)
search_persons.parser.add_argument(
    '--value',
    help='filter by external id value',
)


#
# Run any GET-request with any path with the client
#
@add_command('GET')
def get_request(client, args):
    """ Do any GET-request with any params """
    path = args.path
    logger.info('GET: %r', path)
    url = client.urls.get_href(path.lstrip('/'))
    result = client.get(url)
    return result.json() if result else None


get_request.parser.add_argument('path')
