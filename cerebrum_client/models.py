import json
from datetime import datetime
from enum import Enum
from typing import List, Optional

import pydantic

_type_to_resolver = {
    'person': 'get_person',
    'account': 'get_account'
}


class BaseModel(pydantic.BaseModel):
    @classmethod
    def from_dict(cls, data):
        return cls(**data)

    @classmethod
    def from_json(cls, json_data):
        data = json.loads(json_data)
        return cls.from_dict(data)


class OwnerTypeEnum(str, Enum):
    person = 'person'
    group = 'group'
    account = 'account'


class OwnerRef(BaseModel):
    href: str
    type: OwnerTypeEnum
    id: int
    name: Optional[str]

    def resolve(self, client):
        return getattr(client, _type_to_resolver.get(self.type))(self.id)


class AccountRef(BaseModel):
    href: str
    primary: bool
    id: int
    name: str

    def resolve(self, client):
        return client.get_account(self.name)


class Account(BaseModel):
    contexts: List[str]
    href: str
    name: str
    primary_email: Optional[str]
    expire_date: Optional[datetime]
    owner: OwnerRef
    created_at: datetime
    active: bool
    id: int


class AccountEmailAddressType(BaseModel):
    type: str
    value: str


class AccountPassword(BaseModel):
    password: str


class AccountEmailAddresses(BaseModel):
    primary: str
    addresses: List[AccountEmailAddressType]


class NameVariantEnum(str, Enum):
    first = 'FIRST'
    last = 'LAST'
    full = 'FULL'
    initials = 'INITIALS'
    personaltitle = 'PERSONALTITLE'
    worktitle = 'WORKTITLE'
    middle = 'MIDDLE'    


class OuName(BaseModel):
    variant: str
    name: str
    language: str


class OuContact(BaseModel):
    entity_id: int
    description: Optional[str]
    source_system: str
    value: str
    alias: Optional[str]
    last_modified: Optional[datetime]
    preference: int
    type: str


class OU(BaseModel):
    fakultet: int
    avdeling: int
    institutt: int
    stedkode: str
    names: List[OuName]
    href: str
    id: int
    contact: List[OuContact]
    contexts: List[str]


class OuRef(BaseModel):
    href: str
    id: int


class Affiliation(BaseModel):
    status: str
    create_date: datetime
    source_system: str
    affiliation: str
    last_date: datetime
    ou: OuRef
    deleted_date: Optional[datetime]


class PersonName(BaseModel):
    source_system: str
    variant: NameVariantEnum
    name: str


class Person(BaseModel):
    contexts: List[str]
    created_at: Optional[datetime]
    href: str
    names: List[PersonName]
    birth_date: Optional[datetime]
    id: int


class Contact(BaseModel):
    alias: Optional[str]
    entity_id: int
    last_modified: Optional[datetime]
    preference: int
    source_system: str
    type: str
    value: str
    description: Optional[str]


class Consent(BaseModel):
    expires: Optional[datetime]
    type: str
    name: str
    set_at: Optional[datetime]
    description: str


class PersonExternalId(BaseModel):
    source_system: str
    external_id: str
    id_type: str


class PersonExternalIdSearchResult(BaseModel):
    person_id: int
    source_system: str
    external_id: str
    id_type: str


class Group(BaseModel):
    href: str
    id: int
    name: str
    description: str
    expire_date: Optional[datetime]
    created_at: datetime
    contexts: List[str]
    visibility: str
    moderators: str


class GroupListItem(BaseModel):
    href: str
    id: int
    name: str
    visibility: str
    description: Optional[str]
    created_at: datetime
    expire_date: Optional[datetime]


class GroupMember(BaseModel):
    href: str
    type: str
    id: int
    name: Optional[str]


class QuarantineListItem(BaseModel):
    comment: Optional[str]
    end: Optional[datetime]
    start: datetime
    disable_until: Optional[datetime]
    active: bool
    type: str


class AccountQuarantines(BaseModel):
    quarantines: List[QuarantineListItem]
    locked: bool


class Trait(BaseModel):
    date: Optional[datetime]
    trait: str
    string: Optional[str]
    number: Optional[int]
