#!/usr/bin/env python3
"""Simple command line client to look at SAP data."""
import datetime
import logging
import logging.config
import json

from .cli_commands import parser, run_command
from .client import get_client, merge_dicts
from .models import BaseModel

logger = logging.getLogger(__name__)


default_config = {
    'client': {
        'url': 'http://localhost',
        'headers': {},
    },
    'logging': {
        'version': 1,
        'disable_existing_loggers': False,
        'root': {
            'level': 'DEBUG',
            'handlers': ['default_handler'],
        },
        'handlers': {
            'default_handler': {
                'class': 'logging.StreamHandler',
                'formatter': 'default_formatter',
                'stream': 'ext://sys.stderr',
            },
        },
        'formatters': {
            'default_formatter': {
                'format': '%(levelname)s - %(name)s - %(message)s',
            },
        },
    },
}


def configure_logging(config, args):
    """
    Configure logging.

    Applies logging configuration from config['logging'] if it exists.  If no
    logging section is present in the configuration, the defaults
    (default_log_format, default_log_level) are applied.

    :param dict config:
        A configuration dict, with an optional 'logging' key

    :param argparse.Namespace args:
        CLI arguments, currently ignored.
    """
    log_config = config['logging']
    logging.config.dictConfig(log_config)


def get_config(filename=None):
    """
    Get configuration from a given filename.

    :param str filename: A configuration file to read.
    :rtype: dict
    """
    if filename:
        with open(filename, 'r') as f:
            config = dict(json.load(f))
    else:
        config = {}
    return merge_dicts(default_config, config)


def _make_serializeable(obj):
    """
    Try to make objects json serializable.

    TODO: Should this be a BaseModel-method?  The inverse (i.e. creating dates,
          datetimes from string) is done by this class...
    """
    if isinstance(obj, BaseModel):
        obj = obj.dict()
        for k in obj:
            obj[k] = _make_serializeable(obj[k])
    elif isinstance(obj, (list, tuple)):
        obj = [_make_serializeable(i) for i in obj]
    elif isinstance(obj, (datetime.date, datetime.time)):
        obj = obj.isoformat()
    return obj


def _default_formatter(data):
    """ Format data as JSON strings. """
    data = _make_serializeable(data)
    return json.dumps(data, indent=2)


def get_formatter(args):
    """ Get an output format based on cli args. """
    return _default_formatter


def _default_output(data):
    """ Output method that prints to stdout. """
    print(data)


def get_outputter(args):
    """ Get an output method based on cli args. """
    return _default_output


def main(inargs=None):
    args = parser.parse_args(inargs)

    config = get_config(args.config)
    configure_logging(config, args)

    logger.debug('args: %r', args)

    if not hasattr(args, 'cmd'):
        parser.error('no command given')

    client = get_client(config['client'])
    format_data = get_formatter(args)
    output = get_outputter(args)

    output(format_data(run_command(client, args)))


if __name__ == '__main__':
    parser.prog = 'python -m cerebrum_client'
    main()
