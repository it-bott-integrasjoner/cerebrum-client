#!/usr/bin/env groovy

pipeline {
    agent { label 'python3' }
    options {
        copyArtifactPermission('*');
    }
    stages {
        stage('Run unit tests') {
            steps {
                sh 'tox --recreate'
            }
        }
        stage('Build source distribution') {
            steps {
                sh 'python3.6 setup.py sdist'
                archiveArtifacts artifacts: 'dist/cerebrum-client-*.tar.gz'
            }
        }
        stage('Deploy pkg to Nexus') {
            when { branch 'master' }
            steps {
                build(
                    job: 'python-publish',
                    parameters: [
                        string(name: 'project', value: "${JOB_NAME}"),
                        string(name: 'build', value: "${BUILD_ID}"),
                    ]
                )
            }
        }
    }
    post {
        always {
            junit '**/junit*.xml'
            publishCoverage adapters: [coberturaAdapter(path: '**/coverage*.xml')]
        }
        cleanup {
            sh('rm -vf junit-*.xml')
            sh('rm -vf coverage-*.xml')
            sh('rm -vrf build dist')
        }
    }
}
