import json

import pytest

from cerebrum_client import models


@pytest.fixture
def account_data(base_url):
    return {
        "id": 42,
        "name": "foo",
        "href": "v1/accounts/foo",
        "active": True,
        "primary_email": "foo@example.org",
        "created_at": "2011-11-26T15:02:00.000Z",
        "expire_date": "2012-08-06T05:17:57.000Z",
        "contexts": ['system_foo', 'system_bar'],
        "owner": {
            "href": "string",
            "type": "account",
            "id": 0
        },
    }


@pytest.fixture
def mock_api(endpoints, mock_api, account_data):
    mock_api.get(endpoints.get_account_url('foo'),
                 text=json.dumps(account_data))
    return mock_api


def test_get_account(endpoints, client, mock_api, account_data):
    mock_api.get(
        endpoints.get_account_url('foo'),
        text=json.dumps(account_data))

    assert client.get_account('foo') == models.Account.from_dict(account_data)
