import pytest
import requests_mock


from cerebrum_client.client import CerebrumClient, CerebrumEndpoints


@pytest.fixture
def base_url():
    return 'https://localhost'


@pytest.fixture
def client(base_url):
    return CerebrumClient(base_url)


@pytest.fixture
def endpoints(base_url):
    return CerebrumEndpoints(base_url)


@pytest.fixture
def mock_api(client):
    with requests_mock.Mocker() as m:
        yield m
