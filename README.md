cerebrum-client
===============

cerebrum-client is a Python library
for accessing the [Cerebrum] HTTP [API].


Usage
-----

The client offers both a programmatic API
and a command-line interface client.


### API

```python
from cerebrum_client import CerebrumClient

c = CerebrumClient('https://example.org/', headers={'x-gravitee-api-key': '<my-key>'})
account_data = c.get_account('username')
```


### CLI

```
usage: python -m cerebrum_client [-h] [-v] [-c <json-file>] <command> …

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  -c <json-file>, --config <json-file>
                        read config from <json-file> (default: config.json)

commands:
  Lookups supported by this command line client

  <command>
    get-account         get account info from username
    GET                 Do any GET-request with any params

Examples:
    python -m cerebrum_client get-account example
    python -m cerebrum_client GET /v1/accounts/example
```

A few more examples:

```bash
# Fetch account 'example' with 'get-account'
python -m cerebrum_client get-account example
```

```bash
# Fetch account 'example' with 'GET'
python -m cerebrum_client GET /v1/accounts/example
```


[Cerebrum]: https://www.usit.uio.no/om/tjenestegrupper/cerebrum/
[API]: https://api-uio.intark.uh-it.no/#!/apis?view=results&q=cerebrum
